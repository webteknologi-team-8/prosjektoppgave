let canvas, ctx, fps, 
    gameSpeed = 7,
    points = 0,
    gameState = false,
    gameOver = true,
    pipeGap = 200,
    clicked,
    pipeGame = true,
    highScores = [];
const croc = {
    x: 0,
    y: 0,
    ny: 0, //Nøytral Y posisjon; brukes for å "huske" hvor krokodillens hvilepunkt er
    np: 0,
    height: 33,
    width: 100,
    img: new Image(),
    draw: function() {
        ctx.save();
            ctx.translate(croc.x + croc.width / 2, croc.y + croc.height / 2);
            ctx.rotate(croc.angle * Math.PI/180);
            ctx.drawImage(this.img, 0 - this.width / 2, 0 - this.height / 2, this.width, this.height);
        ctx.restore();
    },
    imgNum: 0,
    dive: false, //Boolsk verdi om den dykker eller ikke
    v: 0, //Krokodillens fart
    a: .1, //Krokodillens akselerasjon
    angle: 0, //Krokodillens vinkel
    passedPipe: 1 //siste pipen krokodillen passerte
};
const water = {
    color: "blue",
    x:0,
    y:0,
    height: 0,
    width: 0,
    img: new Image(),
    draw: function() {
        ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
    }
};
const pipe = {
    0: {
        top: {
            color: "green",
            img: new Image(),
            x: 0,
            y: 0,
            height: 1200,
            width: 60
        },
        bottom: {
            color: "green",
            img: new Image(),
            x: 0,
            y: 0,
            height: 1200,
            width: 60
        },
        draw: function() {
            ctx.drawImage(this.top.img, this.top.x, this.top.y, this.top.width, this.top.height)
            ctx.drawImage(this.bottom.img, this.bottom.x, this.bottom.y, this.bottom.width, this.bottom.height)
        }
    },
    1: {
        top: {
            color: "green",
            img: new Image(),
            x: 0,
            y: 0,
            height: 1200,
            width: 60
        },
        bottom: {
            color: "green",
            img: new Image(),
            x: 0,
            y: 0,
            height: 1200,
            width: 60
        },
        draw: function() {
            ctx.drawImage(this.top.img, this.top.x, this.top.y, this.top.width, this.top.height)
            ctx.drawImage(this.bottom.img, this.bottom.x, this.bottom.y, this.bottom.width, this.bottom.height)
        }
    }
}