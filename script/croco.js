window.onresize = () => {
    sizeCanvas();
    killCroc();
}

function sizeCanvas() {
    canvas = document.getElementById("myCanvas");
    if(window.innerWidth * (8/10) * (6/10) < window.innerHeight - (document.querySelector(".footer").clientHeight + document.querySelector(".navbar").clientHeight)) {
        canvas.width = window.innerWidth * (8/10);
        canvas.height = canvas.width * (6/10);
    } else {
        canvas.height = window.innerHeight - (document.querySelector(".footer").clientHeight + document.querySelector(".navbar").clientHeight);
        canvas.width = canvas.height * (1 / (6/10))
    }
    canvas.onclick = () => {
        if(!gameState && gameOver) {
            start();
        }
    }
}

function start() {

    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    gameState = true;
    gameOver = false;

    if(window.innerWidth * (8/10) * (6/10) < window.innerHeight - (document.querySelector(".footer").clientHeight + document.querySelector(".navbar").clientHeight)) {
        canvas.width = window.innerWidth * (8/10);
        canvas.height = canvas.width * (6/10);
    } else {
        canvas.height = window.innerHeight - (document.querySelector(".footer").clientHeight + document.querySelector(".navbar").clientHeight);
    }

    //Setter opp variablene for Krokodillen som avhenger av skjermstørrelse
    //slik at spillet blir spillbart uansett skjermstørrelse
    croc.x = canvas.width / 25;
    croc.y = canvas.height / 2 - croc.height / 2;
    croc.width = canvas.width / 10
    croc.height = croc.width / 3;
    croc.ny = croc.y;
    croc.img.src = "img/croc_0.png";

    points = 0;
    document.getElementById("poeng").innerHTML = points;

    croc.a = canvas.height / 7000;
    pipeGap = canvas.height / 2.25;
    gameSpeed = canvas.width / (1000 / 7);

    water.img.src = "img/CrocoSplashBackground.png";
    water.height = canvas.height;
    water.width = canvas.width;

    for(let i = 0; i<2; i++) {
        pipe[i].top.img.src = "img/tower0.png";
        pipe[i].bottom.img.src = "img/tower1.png";
        
        //Pipene SKAL beholde samme forhold slik at de ikke blir for små eller store for skjermen
        pipe[i].top.height = canvas.height * (12/7);
        pipe[i].top.width = pipe[i].top.height / 20
        pipe[i].bottom.height = pipe[i].top.height;
        pipe[i].bottom.width = pipe[i].top.width;

        pipe[i].top.x = canvas.width * 1.5;
        pipe[0].top.y = canvas.height - (pipe[0].top.height + canvas.height / 5);
        pipe[1].top.y = -(pipe[1].bottom.height + 10);
        pipe[i].bottom.y = pipe[i].top.y + pipe[i].top.height + pipeGap;
    }
    pipe[1].top.x += canvas.width / 2 + pipe[1].top.width + 200;

    adaptSpeed();

    gameLoop();
    crocAnimate();
}

function gameLoop() {
    if(gameState) {
        //Fjerner alt i canvas og forbereder på neste frame
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        
        water.draw();

        if(pipeGame) {movepipe2()}
        else {movepipe()}

        movecroc();

        croc.draw();
        
        for(let i in pipe) {
        //Dreper Krokodillen
            if(croc.x > pipe[i].top.x - (croc.width * 0.9)  && croc.x < pipe[i].top.x + (pipe[i].top.width - 15)) {
                if(croc.y + croc.height / 10 < pipe[i].top.y + pipe[i].top.height || croc.y > pipe[i].bottom.y - croc.height / 2) {
                    killCroc();
                }
            }

        //Teller poeng
            if(croc.x > pipe[i].top.x + pipe[i].top.width) {
                if(i != croc.passedPipe) {
                    croc.passedPipe = i;
                    if(gameSpeed <= 10) {
                        gameSpeed *= 1.003;
                        croc.a *= 1.003;
                    }
                    points++;
                    if(points >= 15) {
                        pipeGame = false;
                    }
                    document.getElementById("poeng").innerHTML = points;
                }
            }
        }

        //Kjører loopen opptil like mange ganger i sekundet som skjermen din oppdaterer seg selv
        requestAnimationFrame(gameLoop);
}
}

function movepipe() {
    for(let i in pipe) {
        pipe[i].top.x -= gameSpeed * (60 / fps);
        pipe[i].bottom.x = pipe[i].top.x;
        pipe[i].draw();
        //////////////////////////////////////////////////////////////////////
        //Flytter en pipe fra venstre til høyre
        if(pipe[i].top.x < -(pipe[i].top.width + 10)) {
            pipeGap = canvas.height / 3;
            if(points <=40) {
                pipe[i].top.x = canvas.width + (400 - points * 10);
            } else {
                pipe[i].top.x = canvas.width;
            }
            //
            pipe[i].top.y = Math.round(Math.random() * (canvas.height - 200)) - pipe[i].top.height + 50;
            pipe[i].bottom.y = pipe[i].top.y + pipe[i].top.height + pipeGap;
        }
    }
}

function movepipe2() {
    for(let i in pipe) {
        pipe[i].top.x -= gameSpeed * (60 / fps);
        pipe[i].bottom.x = pipe[i].top.x;
        pipe[i].draw();
        //////////////////////////////////////////////////////////////////////
        //Flytter en pipe fra venstre til høyre
        if(pipe[i].top.x < -(pipe[i].top.width + 10)) {
            pipeGap = canvas.height;
            if(points <=40) {
                pipe[i].top.x = canvas.width + (400 - points * 10);
            } else {
                pipe[i].top.x = canvas.width;
            }
            //Bestemmer om pipen skal være opp eller ned, og hvor mye
            let upDown = Math.round(Math.random() * 1);
            pipe[i].top.y = (upDown) ? Math.round(Math.random() * (canvas.height / 2)) + canvas.height / 2 - (pipe[i].top.height + croc.height * 1.2) : 
                Math.round(Math.random() * (canvas.height / 2)) - canvas.height - pipe[i].top.height + croc.width;
            pipe[i].bottom.y = pipe[i].top.y + pipe[i].top.height + pipeGap;
        }
    }
}

function movecroc() { 
    //Her beveger krokodillen seg slik at når den treffer vannet igjen etter å ha hoppet, stopper den
    //så lenge spilleren ikke holder inn space-knappen
    if(croc.dive) {
        if(croc.y < canvas.height - croc.height) { //Passer på at krokodillen ikke overskrider canvas høyden
            if(croc.y >= croc.ny) {
                croc.v += croc.a * 2;
            } else if(croc.y < croc.ny) {
                if(croc.v <= 0) {
                    croc.v = 0;
                }
                croc.v += croc.a * 4;
            }
        } else {
            croc.v = 0;
            croc.y = canvas.height - croc.height;
        }
    } else if(!croc.dive) {
        if(croc.y < croc.ny) {
            if(croc.y < 0) {
                croc.v = 0;
                croc.y = 0;
            }
            croc.v += croc.a * 4.4;
        } else if(croc.y > croc.ny + 20 && croc.v > 0) {
            croc.v = 0;
        } else if(croc.y > croc.ny + 20) {
            croc.v -= croc.a * 4.7;
        } else if(croc.y < croc.ny + 20 && croc.y > croc.ny - 20 && croc.v >= 0) {
            croc.v = 0;
        }
    }
    croc.y += croc.v * (60 / fps);
    croc.angle = croc.v;
}

window.onmousedown = () => {dive();}
window.onmouseup = () => {surface();}
window.onkeydown = (e) => {
    if(e.keyCode == 13) {
        if(!gameState && gameOver) {
            start();
        }
        dive();
    }
}
window.onkeyup = (e) => {
    if(e.keyCode == 13) {
        surface();
    }}

function dive() {
    croc.dive = true;
    croc.imgNum = 0;
}
function surface() {
    croc.dive = false;
}

function crocAnimate() {
    //3 States of swimming 
    setTimeout(
        () => {
            if(croc.v != 0) {
                if(croc.y >= canvas.height/ 2 - croc.height / 2) {
                    //State 1:
                    //When swimming underwater, fast movements
                    switch(croc.imgNum) {
                        case 0:
                            croc.img.src = "img/croc_0.png";
                            croc.imgNum++;
                            break;
                        case 1:
                            croc.img.src = "img/croc_1.png";
                            croc.imgNum++;
                            break;
                        case 2:
                            croc.img.src = "img/croc_2.png";
                            croc.imgNum = 0;
                            break;
                        default:
                            croc.img.src = "img/croc_2.png";
                            croc.imgNum++;
                            break;
                    }   
                } else {
                    //State 2:
                    //When leaping, keep legs tucked back
                    croc.imgNum = 0;
                    croc.img.src = "img/croc_2.png";
                }
            } else {
                //State 3:
                //When idle, slowly pushes along the water;
                switch(croc.imgNum) {
                    case 0:
                        croc.img.src = "img/croc_0.png";
                        croc.imgNum++;
                        break;
                    case 1:
                        croc.img.src = "img/croc_1.png";
                        croc.imgNum++;
                        break;
                    case 6:
                        croc.img.src = "img/croc_2.png";
                        croc.imgNum = 0;
                        break;
                    default:
                        croc.img.src = "img/croc_2.png";
                        croc.imgNum++;
                        break;
                }   

            }
            if(gameState) {
                crocAnimate();
            } else {
                croc.img.src = "img/croc_2.png";
            }
        }, 150
    )
}

function killCroc() {
    gameState = false;
    croc.v = 0;
    highScore();
    setTimeout(() => {
        for(let i in pipe) {
            pipe[i].top.x = -(pipe[i].width * 2);
            pipe[i].bottom.x = pipe[i].top.x;
            pipe[i].draw();
        }
        deadFall();
        function deadFall() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            croc.v += croc.a;
            croc.y += croc.v;
            if(croc.angle <= 90 && croc.angle >= 80) {croc.angle = 80 + croc.v;}
            else if(croc.angle > 90) {
                croc.angle = 90;
            }
            else {croc.angle += 1.2}
            water.draw();
            croc.draw();
            if(croc.y >= canvas.height - croc.height) {
                //Legg inn resetfunkjon her
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                getHighScores();
                pipeGame = true;
                return gameOver = true;
            }
            requestAnimationFrame(deadFall);
        }
    }, 10);   

    gameState = false;
}

function highScore() {
    if(!gameState) {
        if(localStorage.getItem("highScore") <= points) {
            localStorage.setItem("highScore", points);
        }
    }
}

function adaptSpeed() { //Finner ut hvor ofte skjermen din oppdaterer seg selv slik at spillet kan bestemme egen hastighet ut ifra dette
    let frame = 0;
    let lastTime = Date.now();

    let loop = function() {
        let now = Date.now();
        fps = Math.round(1000 / (now - lastTime));

        lastTime = now;
        frame++;

        if (now > 1000 + lastTime) {
            fps = Math.round((frame * 1000) / (now - lastTime));
            frame = 0;
            lastTime = now;
        };

        requestAnimationFrame(loop);
    }

    loop();
}
