//Code to get the pictures to become clear upon mouseenter and mouseleave
$(document).ready (function() {
    $(".crocpictures").fadeTo (0, 0.5);

    $ ("#aboutandrea").mouseenter (function(){
        $("#andreacroc").animate({opacity: "1"});
    })
    
    $ ("#aboutandrea").mouseleave (function(){
        $("#andreacroc").animate ({opacity: 0.5});
    })
    
    $ ("#aboutedwin").mouseenter (function(){
        $("#edwincroc").animate({opacity: "1"}); 
    })
    
    $ ("#aboutedwin").mouseleave (function(){
        $("#edwincroc").animate ({opacity: 0.5});
    })
    
    $ ("#aboutper").mouseenter (function(){
        $("#percroc").animate({opacity: "1"});
    })
    
    $ ("#aboutper").mouseleave (function(){
        $("#percroc").animate ({opacity: 0.5});
    })
    
    $ ("#aboutruben").mouseenter (function(){
        $("#rubencroc").animate({opacity: "1"}); 
    })
    
    $ ("#aboutruben").mouseleave (function(){
        $("#rubencroc").animate ({opacity: 0.5});
    })
})
